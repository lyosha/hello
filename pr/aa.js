var enabled = true;

function round(root) {
    if (!enabled)
        return false;

    var filter = function(v) {
        var beforeNine = true;
        var v2 = [];
        var lastIndex = -1;
        for (var i = 0; i < v.length; i++) {
            var c = v[i];
            if (beforeNine) {
                if (/[0-8]/.exec(c) || (c == '9' && lastIndex == -1)) {
                    lastIndex = i;
                } else if (c == '9') {
                    beforeNine = false;
                    if (lastIndex == -1) {
                        throw "First nine should have been ignored.";
                    } else {
                        c = '0';
                        v2[lastIndex] = (parseInt(v2[lastIndex]) + 1).toString();
                    }
                }
            } else {
                c = c.replace(/[0-9]/, '0');
            }
            v2.push(c);
        }

        return v2.join('');
    }

    var tw = document.createTreeWalker(root, NodeFilter.SHOW_TEXT);
    var from = [];
    var r = /(\$[0-9][0-9,.]+)/
        while (n = tw.nextNode()) {
            if (n.parentElement.tagName == "SCRIPT" || !r.exec(n.nodeValue))
                continue;
            from.push(n);
        }

    if (from.length == 0)
        return false;

    var ret = false;
    from.forEach(function(n) {
        var s = n.nodeValue.split(r);
        var left = s[0];
        var changed = false;
        for (var i = 1; i < s.length; i += 2) {
            var x = s[i];
            var xf = filter(x);
            if (x === xf) {
                left += x + s[i+1];
                continue;
            }
            ret = changed = true;
            n.parentNode.insertBefore(document.createTextNode(left), n);
            var t = document.createElement('i');
            t.title = x;
            t.setAttribute('class', 'nnn');
            t.textContent = xf;
            n.parentNode.insertBefore(t, n);
            left = s[i+1];
        }
        if (changed) {
            n.parentNode.insertBefore(document.createTextNode(left), n);
            n.parentNode.removeChild(n);
        }
    });

    return ret;
}

function undo() {
    var forEach = Array.prototype.forEach;
    forEach.call(document.querySelectorAll('i.nnn'), function(n) {
        n.parentNode.replaceChild(document.createTextNode(n.title), n);
    });
}

if (round(document.body))
    chrome.extension.sendRequest({}, function(response) {});

chrome.extension.onRequest.addListener(function(request, sender, sendResponse) {
    enabled = !enabled;
    if (enabled)
        round(document.body);
    else
        undo();
    sendResponse(enabled);
});

window.addEventListener('DOMNodeInserted', function(event) {
    round(event.target);
}, false);
